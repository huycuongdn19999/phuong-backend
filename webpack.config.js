const path = require('path')
const webpackMerge = require('webpack-merge')
const pluginWebpackNodeExternals = require('webpack-node-externals')

const configCommon = require('./webpack.config.common')
const configDev = require('./webpack.config.dev')

const baseConfig = {
  entry: './main.js',
  output: {
    path: path.resolve('dist'),
    filename: 'server.bundle.js',
  },
  target: 'node',
  externals: [pluginWebpackNodeExternals()],
}

module.exports = (env) => {
  if (env.development) {
    return webpackMerge(baseConfig, configCommon, configDev)
  }
  return webpackMerge(baseConfig, configCommon)
}
