import md5Model from './../models/md5'


const Md5Dao = {
    async create(data) {
        const result = md5Model.create(data)
        return result
    },
    async findOne(query = {}) {
        return md5Model.findOne(query)
    }
}
export default Md5Dao
