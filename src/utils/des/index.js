const Encryption = require('node_triple_des');

import randomstring  from 'randomstring' 

export const genKey= () => {
    const key = randomstring.generate();
    return key;
}
export const encryptText = (text, SharedKey) => {
    const encrypted =  Encryption.encrypt(SharedKey, text);
    return encrypted
}
export const decryptText = (encryptText,SharedKey) => {
    try {
        const decrypted =  Encryption.decrypt(SharedKey, encryptText);
        return decrypted
    } catch (error) {
        return null
    }
}