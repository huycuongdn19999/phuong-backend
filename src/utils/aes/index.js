import Cipher from 'aes-ecb'
import randomstring from 'randomstring'

export const genKey = (size) => {
    const number = parseInt(size);
    const key = randomstring.generate(number / 8);
    return key;
}
export const encode = (text, key) => {

    console.log({ text, key })
    // key = 'KeyMustBe16ByteOR24ByteOR32Byte!'
    let result = Cipher.encrypt(key, text);
    return result
}

export const decode = async (text, key) => {
    // console.log({ text, key })
    // key = 'KeyMustBe16ByteOR24ByteOR32Byte!'

    let result = await Cipher.decrypt(key, text);
    console.log(JSON.stringify(result), 'resultresultresultresultresult')
    return result
}