const NodeRSA = require('node-rsa');


export const genKey = () => {
    const key = new NodeRSA({ b: 1024 });
    const publicKey = key.exportKey('public')
    const privateKey = key.exportKey()
    return {
        publicKey,
        privateKey
    }
}

export const encryptText = (text, publicKey) => {
    const key = new NodeRSA()
    key.importKey(publicKey, 'public')
    const encrypted = key.encrypt(text, 'base64');

    return encrypted
}
export const decryptText = (encryptText, privateKey) => {
    try {
        const key = new NodeRSA()
        key.importKey(privateKey, 'private')
        const decrypted = key.decrypt(encryptText, 'utf8');
        return decrypted
    } catch (error) {
        return null
    }
}

