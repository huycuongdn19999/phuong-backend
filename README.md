## How start project
- dev: `yarn run:dev`
- build production: `yarn build:prod`


# Giới thiệu
API core liên quan đến tương tác dữ liệu với trạm quan trắc, với các epic sau

* api-category
* api-station-auto
* api-data-station-auto
* api-station-fixed
* api-data-station-fixed
* api-aqi
* api-wqi

# Port

Port mặc định là `5001`

# Environment
```env
NODE_ENV = development | production
```

```bash
MONGODB_HOST=52.163.249.130:27017
MONGODB_DATABASE=ilotusland_stnmt_hanoi
MONGODB_USER=dev
MONGODB_PASS=506b04d39d168bbfa3725221c1522d04

# internal api
HOST_AUTH=https://dev-api.ilotusland.asia
HOST_FTP=https://dev-api.ilotusland.asia
HOST_PUBLIC=http://localhost:5001
HOST_AMQP=amqp://guest:guest@52.163.249.130:5672/ilotusland
HOST_CATEGORY=http://localhost:5001
STATION_AUTO_API=http://localhost:5001
HOST_PUBLIC=http://localhost:5001
HOST_AQI=http://localhost:5001

# api get adress
CATEGORY_LOCATION_API_URL=https://api.ipgeolocation.io/ipgeo
CATEGORY_LOCATION_API_KEY=dfc572b4ff4143629ed1e1120f6f572b
```

# Init
 - git submodule init
 - git submodule update --remote

# Editor

 - VSCode


## Format code
 - Eslint + Prettier
### rules
- dùng ký tự khoảng trắnng khi nhấn tab
- prettier, airbnb

# Library
 - restify

# Notes
- có thể dùng `_` ở bất kỳ file nào mà không cần `import _ from 'lodash'`
- truy cập nhanh đến các top folder trong folder `src` bằng `~<folder_name>` (vd: dùng `~templates` khi cần truy cập đến folder `src/templates`)
- khi nhấn `tab` sẽ dùng ký tự khoảng trắng thay vì ký tự tab (do các file yaml không hiểu ký tự tab)
- syntaxs:
  - có thêm operators: `nullist`, `optional chaining`
